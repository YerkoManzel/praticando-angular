import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {UserCardResponse} from '../properties/response/user-card.response';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnChanges {
  @Input() public userCard: UserCardResponse;
  @Input() public userCardSelected: UserCardResponse;

  @Output() public selectedCardEmit: EventEmitter<UserCardResponse>;

  constructor() {
    this.selectedCardEmit = new EventEmitter<UserCardResponse>();
  }

  public ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

  public selectedCard(userCard: UserCardResponse) {
    this.selectedCardEmit.emit(userCard);
  }
}
