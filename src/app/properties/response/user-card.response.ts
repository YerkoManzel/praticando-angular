export class UserCardResponse {
  public firstName: string;
  public lastName: string;
  public role: string;
  public description: string;
}
