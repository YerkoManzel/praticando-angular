import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {RoleEnum} from '../properties/enum/role.enum';
import {UserCardResponse} from '../properties/response/user-card.response';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit, OnDestroy, OnChanges {
  public userCards: UserCardResponse[];

  @Input() public userCardSelected: UserCardResponse;

  constructor() {
    this.userCardSelected = new UserCardResponse();
    this.userCards = [
      {
        firstName: 'jhon',
        lastName: 'smith',
        role: RoleEnum.Creative_Director,
        description: 'Hola soy una persona que le gusta amanecer trabajando y no me gusta dormir.'
      },
      {
        firstName: 'diego',
        lastName: 'mani',
        role: RoleEnum.Developer,
        description: 'Hola soy una persona que le gusta amanecer trabajando y no me gusta dormir.'
      },
      {
        firstName: 'carlos',
        lastName: 'Claros',
        role: RoleEnum.Team_Lead,
        description: 'Hola soy una persona que le gusta amanecer trabajando y no me gusta dormir.'
      }
    ];
  }

  public ngOnInit(): void {
    console.log('ngOnInit');
  }

  public ngOnDestroy(): void {
    console.log('ngOnDestroy');
  }

  public ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

  public selectedCard(userCard: UserCardResponse) {
    this.userCardSelected = userCard;
  }
}
